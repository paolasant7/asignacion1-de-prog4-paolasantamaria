import sqlite3

conn = sqlite3.connect('asign1ps.db')


c = conn.cursor()


def creartablas():
    tabla = [""" CREATE TABLE diccionariops(
        ID INTEGER PRIMARY KEY AUTOINCREMENT, 
        PALABRA TEXT NOT NULL,
        SIGNIFICADO TEXT NOT NULL
        );
        """
    ]      
conn.commit() 

def Principal():
    creartablas()
    
    MENU = """
a) Agregar una nueva Palabra.
b) Editar alguna Palabra existente.
c) Eliminar alguna Palabra existente.
d) Ver listado de Palabras.
e) Buscar significado de X Palabra.
f) Salir.
Elige: """
    elec = ""
    while elec != "f":
        elec = input(MENU)

        if elec == "a":
            PALABRA = input("Ingrese la palabra: ")
            # Se comprueba para ver si no existe
            posible_significado = buscar_significado_palabra(PALABRA)
            if posible_significado:
                print(f"La palabra '{PALABRA}' ya existe.")
            else:
                SIGNIFICADO = input("Ingresa el significado: ")
                agregarp(PALABRA, SIGNIFICADO)
                print("Palabra Agregada satisfactoriamente!")

        if elec == "b":
            PALABRA = input("Ingrese la palabra que quieres editar: ")
            nuevosign = input("Ingrese el nuevo significado: ")
            editarp(PALABRA, nuevosign)
            print("Palabra Actualizada satisfactoriamente!")

        if elec == "c":
            PALABRA = input("Ingrese la palabra a eliminar: ")
            eliminarp(PALABRA)

        if elec == "d":
            palabras = obtenerp()
            print("=== Lista de Palabras ===")
            for PALABRA in palabras:
                print(PALABRA[0])

        if elec == "e":
            PALABRA = input(
                "Ingresa la Palabra de la cual solicita saber el significado: ")
            SIGNIFICADO = buscar_significado_palabra(PALABRA)
            if SIGNIFICADO:
                print(f"El significado de '{PALABRA}' es:\n{SIGNIFICADO[0]}")
            else:
                print(f"La Palabra '{PALABRA}' no fue encontrada")
    

                
    

def agregarp(PALABRA, SIGNIFICADO):
    c = conn.cursor()
    sent = "INSERT INTO diccionariops (PALABRA, SIGNIFICADO) VALUES (?, ?)"
    c.execute(sent, [PALABRA, SIGNIFICADO])
    conn.commit()

def editarp(PALABRA, nuevosign):
    c = conn.cursor()
    sent = "UPDATE diccionario SET SIGNIFICADO = ? WHERE PALABRA = ?"
    c.execute(sent, [nuevosign, PALABRA])
    conn.commit()

def eliminarp(PALABRA):
    c = conn.cursor()
    sent = "DELETE FROM diccionariops WHERE PALABRA = ?"
    c.execute(sent, [PALABRA])
    conn.commit()

def obtenerp():
    c = conn.cursor()
    consult = "SELECT PALABRA FROM diccionariops"
    c.execute(consult)
    return c.fetchall()

def buscar_significado_palabra(PALABRA):
    c = conn.cursor()
    consult = "SELECT SIGNIFICADO FROM diccionariops WHERE PALABRA = ?"
    c.execute(consult, [PALABRA])
    return c.fetchone()

if __name__ == '__main__':
    Principal()
  

 
conn.close()